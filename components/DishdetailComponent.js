import React, { Component } from 'react';
import { Text, View, ScrollView, StyleSheet, FlatList, Modal, Button, Alert, PanResponder, Share } from 'react-native';
import { Card, Icon, Input } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postFavorite, postComment } from '../redux/ActionCreators';
import { Rating } from 'react-native-ratings';
import * as Animatable from 'react-native-animatable';
import { getSupportedVideoFormats } from 'expo/build/AR';


const mapStateToProps = state => {
    return {
      dishes: state.dishes,
      comments: state.comments,
      favorites: state.favorites
    }
  }

const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment))
})

function RenderDish(props) {

    const dish = props.dish;

    handleViewRef = ref => this.view = ref;

    const recognizeDrag = ({moveX, moveY, dx, dy}) => {
        if ( dx < -200 )
            return true;
        else
            return false;
    };

    const recognizeComment = ({moveX, moveY, dx, dy}) => {
        if( dx > 200) 
            return true;
        else
            return false;
    };

    const panResponder = PanResponder.create({ //for gestures
        onStartShouldSetPanResponder: (e, gestureState) => {
            return true;
        },
        onPanResponderGrant: () => {this.view.rubberBand(1000).then(endState => console.log(endState.finished ? 'finished' : 'cancelled'));},
        onPanResponderEnd: (e, gestureState) => {
            if(recognizeDrag(gestureState))
                Alert.alert(
                    'Add to Favorites',
                    'Are you sure you wish to add ' +dish.name + ' to your Favorites?',
                    [
                        {
                            text: 'Cancel',
                            onPress: () => console.log('Cancel pressed'),
                            style: 'cancel'
                        },
                        {
                            text: 'Ok',
                            onPress: () => props.favorite ? console.log('Already favourite') : props.onPress()
                        }
                    ],
                    { cancelable: false }
                );
            else if (recognizeComment(gestureState)) props.toggleModal();
            return true;
        }
    });

    const shareDish = (title, message, url) => {
        Share.share({
            title: title,
            message: title + ' :' + message + ' ' + url,
            url: url
        }, {
            dialogTitle: 'Share ' + title
        });
    }
    
        if (dish != null) {
            return(
                <Animatable.View animation="fadeInDown" duration={2000} delay={1000}
                ref={this.handleViewRef}
                {...panResponder.panHandlers}>
                <Card
                featuredTitle={dish.name}
                image={{uri: baseUrl + dish.image}}>
                    <Text style={{margin: 10}}>
                        {dish.description}
                    </Text>
                    <View style={styles.cardRow}>
                    <Icon 
                        raised //make it as a button
                        reverse //colors
                        name={ props.favorite ? 'heart' : 'heart-o'}
                        type='font-awesome'
                        color='#f50'
                        style={styles.cardItem}
                        onPress={() => props.favorite ? console.log('Already favourite') : props.onPress()}
                    />
                    <Icon 
                        raised 
                        reverse
                        type='font-awesome'
                        name='pencil'
                        style={styles.cardItem}
                        onPress={() => props.toggleModal()}
                        
                    />
                    <Icon 
                    raised
                    reverse
                    name='share'
                    type='font-awesome'
                    color='#51D2A8'
                    style={styles.cardItem}
                    onPress={() => shareDish(dish.name, dish.description, baseUrl + dish.image)}
                    />
                    </View>
                </Card>
                </Animatable.View>
            );
        }
        else {
            return(<View></View>);
        }
}

function RenderComments(props) {
    const comments = props.comments;

    const renderCommentItem = ({item, index}) => {
        return(
            <View key={index} style={{margin:10}} >
                <Text style={{fontSize: 14}}>{item.comment}</Text>
                <Rating readonly startingValue={item.rating} />
                <Text style={{fontSize: 12}}>{'-- ' + item.author + ', ' + item.date}</Text>
            </View>
        );
    }

    return(
        <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>
            <Card title="Comments">
                <FlatList 
                    data={comments}
                    renderItem={renderCommentItem}
                    keyExtractor={item => item.id.toString()}
                />
            </Card>
        </Animatable.View>
    );
}


class Dishdetail extends Component {

    constructor (props) {
        super(props);

        this.state = {
            rating: 1,
            author: '',
            comment: '',
            showModal: false
        }
    }

    toggleModal() {
        this.setState({ showModal: !this.state.showModal })
    }

    resetForm() {
        this.setState({
            rating: 1,
            author: '',
            comment: ''
        });
    }

    markFavorite(dishId) {
        this.props.postFavorite(dishId);
    }

    handleComment(dishId, rating, author, comment) {
        this.props.postComment(dishId, rating, author, comment),
        this.toggleModal();
    }

    static navigationOptions = {
        title: 'Dish Details'
    };

    render() {
        const dishId = this.props.navigation.getParam('dishId', '');

        return(
            <ScrollView>
                <RenderDish dish={this.props.dishes.dishes[+dishId]}
                    favorite={this.props.favorites.some(el => el === dishId)}
                    onPress={() => this.markFavorite(dishId)} 
                    toggleModal={() => this.toggleModal()}
                    />
                <RenderComments comments={this.props.comments.comments.filter((comment) => comment.dishId === dishId)} />

                <Modal 
                    animationType={'slide'}
                    transparent={false}
                    visible={this.state.showModal}
                    onDismiss={() => {this.toggleModal(); this.resetForm()}}
                    onRequestClose={() => {this.toggleModal(); this.resetForm()}}
                >
                    <ScrollView style={styles.modal} >

                        <View style={styles.formRow} >
                            <Rating 
                                style={styles.formItem}
                                ratingCount = {5}
                                showRating
                                onFinishRating={(rating) => this.setState({rating: rating})}
                            />
                        </View>
                        
                        <View style={styles.formRow} >
                            <Input
                            style={styles.formItem}
                            placeholder='Author'
                            leftIcon={{ type: 'font-awesome', name: 'user' }}
                            onChangeText={(text) => this.setState({author: text})}
                            />
                        </View>
                        
                        <View style={styles.formRow} >
                            <Input
                            style={styles.formItem}
                            placeholder='Comment'
                            leftIcon={{ type: 'font-awesome', name: 'comment' }}
                            onChangeText={(text) => this.setState({comment: text}) }
                            />
                        </View>

                        <View style={styles.formRow}>
                            <Button
                                onPress={() => {this.handleComment(dishId, this.state.rating, this.state.author, this.state.comment); this.toggleModal()} }
                                title="Submit"
                                color="#512DA8"
                                accessibilityLabel="submit"
                                />
                        </View>

                        <View style={styles.formRow}>
                            <Button
                                onPress={() => {this.toggleModal(); this.resetForm()}}
                                title="Cancel"
                                accessibilityLabel="cancel"
                                />
                        </View>

                    </ScrollView>
                </Modal>

            </ScrollView>
        );
    }
   
}

const styles = StyleSheet.create({
    formRow: {
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
      flexDirection: 'row',
      margin: 20
    },
    formItem: {
        flex: 1
    },
    modal: {
        margin: 20
    },
    modalTitle: {
        fontSize: 24,
        fontWeight: 'bold',
        backgroundColor:'#512DA8',
        textAlign: 'center',
        color: 'white',
        marginBottom: 20
    },
    modalText: {
        fontSize: 10,
        margin: 10
    },
    cardRow: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        flexDirection: 'row',
    },
    cardItem: {
        flex: 1
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Dishdetail);